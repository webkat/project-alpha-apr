from django.shortcuts import render
from .models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def show_my_tasks(request):
    if request.method == "POST":
        tasks = Task.objects.filter(owner=request.user)
        context = {
            "tasks": tasks,
        }
    return render(request, "tasks/list.html", context)
